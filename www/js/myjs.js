window.fillConfig = null;
window.saveConfig =null;
window.saveAccount =null;

 /* 此JS文件是格式化JS中日期时间的工具类，其中包含了传入日期对象Date，格式化成想要的格式，<br>  
 * 或者传入字符串格式的时间个，次字符串日期对应的格式可以转换为相应的日期对象，<br>  
 * 可以计算两个日期之间的差值  
 *   
 * y: 表示年  
 * M：表示一年中的月份 1~12  
 * d: 表示月份中的天数 1~31  
 * H：表示一天中的小时数 00~23  
 * m: 表示小时中的分钟数 00~59  
 * s: 表示分钟中的秒数   00~59  
 */    
      
  var DateFormat = function(bDebug){    
      this.isDebug = bDebug || false;    
      this.curDate = new Date();    
  }    
     
 DateFormat.prototype = {    
    //定义一些常用的日期格式的常量     
    DEFAULT_DATE_FORMAT: 'yyyy-MM-dd',    
    DEFAULT_MONTH_FORMAT: 'yyyy-MM',    
    DEFAULT_YEAR_FORMAT: 'yyyy',    
    DEFAULT_TIME_FORMAT: 'HH:mm:ss',    
    DEFAULT_DATETIME_FORMAT: 'yyyy-MM-dd HH:mm:ss',    
    DEFAULT_YEAR: 'YEAR',    
    DEFAULT_MONTH: 'MONTH',    
    DEFAULT_DATE: 'DATE',    
    DEFAULT_HOUR: 'HOUR',    
    DEFAULT_MINUTE: 'MINUTE',    
    DEFAULT_SECOND: 'SECOND',    
        
    /**  
     * 根据给定的日期时间格式，格式化当前日期  
     * @params strFormat 格式化字符串， 如："yyyy-MM-dd" 默认格式为：“yyyy-MM-dd HH:mm:ss”  
     * @return 返回根据给定格式的字符串表示的时间日期格式<br>  
     *         如果传入不合法的格式，则返回日期的字符串格式{@see Date#toLocaleString()}  
     */    
     formatCurrentDate: function(strFormat){    
       try{    
          var tempFormat = strFormat == undefined? this.DEFAULT_DATETIME_FORMAT: strFormat;    
          var dates = this.getDateObject(this.curDate);    
          if(/(y+)/.test(tempFormat)){    
             var fullYear = this.curDate.getFullYear() + '';    
             var year = RegExp.$1.length == 4? fullYear: fullYear.substr(4 - RegExp.$1.length);    
             tempFormat = tempFormat.replace(RegExp.$1, year);    
          }    
          for(var i in dates){    
             if(new RegExp('(' + i + ')').test(tempFormat)){    
                var target = RegExp.$1.length == 1? dates[i]: ('0' + dates[i]).substr(('' + dates[i]).length - 1);    
                tempFormat = tempFormat.replace(RegExp.$1, target);    
             }    
          }    
          return tempFormat === strFormat? this.curDate.toLocaleString(): tempFormat;    
       }catch(e){    
          this.debug('格式化日期出现异常：' + e.message);    
       }    
    },    
        
        
    /**  
     * 根据给定的格式，把给定的时间进行格式化  
     * @params date 要格式化的日期  
     * @params strFormat 要得到的日期的格式的格式化字符串，如：'yyyy-MM-dd'，默认：yyyy-MM-dd HH:mm:ss  
     * @return 根据规定格式的时间格式  
     *    
     * @updateDate 2011-09-15  
     */    
    format: function(date, strFormat){    
     try{    
        if(date == undefined){    
           this.curDate = new Date();    
        }else if(!(date instanceof Date)){    
           this.debug('你输入的date:' + date + '不是日期类型');    
           return date;    
        }else{    
           this.curDate = date;    
        }    
        return this.formatCurrentDate(strFormat);    
     }catch(e){    
        this.debug('格式化日期出现异常：' + e.message);    
     }    
    },    
        
    /**  
     * 根据给定的格式对给定的字符串日期时间进行解析，  
     * @params strDate 要解析的日期的字符串表示,此参数只能是字符串形式的日期，否则返回当期系统日期  
     * @params strFormat 解析给定日期的顺序, 如果输入的strDate的格式为{Date.parse()}方法支持的格式，<br>  
     *         则可以不传入，否则一定要传入与strDate对应的格式, 若不传入格式则返回当期系统日期。  
     * @return 返回解析后的Date类型的时间<br>  
     *        若不能解析则返回当前日期<br>  
     *        若给定为时间格式 则返回的日期为 1970年1月1日的日期  
     *  
     * bug: 此方法目前只能实现类似'yyyy-MM-dd'格式的日期的转换，<br>  
     *       而'yyyyMMdd'形式的日期，则不能实现  
     */    
         
    parseDate: function(strDate, strFormat){    
       if(typeof strDate != 'string'){    
            return new Date();    
       }    
      var longTime = Date.parse(strDate);    
      if(isNaN(longTime)){    
          if(strFormat == undefined){    
              this.debug('请输入日期的格式');    
             return new Date();    
          }    
          var tmpDate = new Date();    
          var regFormat = /(\w{4})|(\w{2})|(\w{1})/g;    
          var regDate = /(\d{4})|(\d{2})|(\d{1})/g;    
          var formats = strFormat.match(regFormat);    
          var dates = strDate.match(regDate);    
          if( formats != undefined &&  dates != undefined && formats.length == dates.length){    
            for(var i = 0; i < formats.length; i++){    
              var format = formats[i];    
              if(format === 'yyyy'){    
                tmpDate.setFullYear(parseInt(dates[i], 10));    
              }else if(format == 'yy'){    
                var prefix = (tmpDate.getFullYear() + '').substring(0, 2);    
                var year = (parseInt(dates[i], 10) + '').length == 4? parseInt(dates[i], 10): prefix + (parseInt(dates[i], 10) + '00').substring(0, 2);    
                var tmpYear = parseInt(year, 10);    
                tmpDate.setFullYear(tmpYear);    
              }else if(format == 'MM' || format == 'M'){    
                tmpDate.setMonth(parseInt(dates[i], 10) - 1);    
              }else if(format == 'dd' || format == 'd'){    
                tmpDate.setDate(parseInt(dates[i], 10));    
              }else if(format == 'HH' || format == 'H'){    
                tmpDate.setHours(parseInt(dates[i], 10));    
              }else if(format == 'mm' || format == 'm'){    
                tmpDate.setMinutes(parseInt(dates[i], 10));    
              }else if(format == 'ss' || format == 's'){    
                tmpDate.setSeconds(parseInt(dates[i], 10));    
              }    
            }    
           return tmpDate;    
         }    
          return tmpDate;    
        }else{    
          return new Date(longTime);    
        }    
    },    
        
        
    /**  
     * 根据给定的时间间隔类型及间隔值，以给定的格式对给定的时间进行计算并格式化返回  
     * @params date 要操作的日期时间可以为时间的字符串或者{@see Date}类似的时间对象，  
     * @params interval 时间间隔类型如："YEAR"、"MONTH"、 "DATE", 不区分大小写  
     * @params amount 时间间隔值，可以正数和负数, 负数为在date的日期减去相应的数值，正数为在date的日期上加上相应的数值  
     * @params strFormat 当输入端的date的格式为字符串是，此项必须输入。若date参数为{@see Date}类型是此项会作为最终输出的格式。  
     * @params targetFormat 最终输出的日期时间的格式，若没有输入则使用strFormat或者默认格式'yyyy-MM-dd HH:mm:ss'  
     * @return 返回计算并格式化后的时间的字符串  
     */    
    changeDate: function(date, interval, amount, strFormat, targetFormat){    
        var tmpdate = new Date();    
        if(date == undefined){    
           this.debug('输入的时间不能为空!');    
           return new Date();    
        }else if(typeof date == 'string'){    
            tmpdate = this.parseDate(date, strFormat);    
        }else if(date instanceof Date){    
          tmpdate = date;    
        }    
        var field  =  (typeof interval == 'string')? interval.toUpperCase(): 'DATE';    
            
        try{    
          amount = parseInt(amount + '', 10);    
          if(isNaN(amount)){    
             amount = 0;    
          }    
        }catch(e){    
           this.debug('你输入的[amount=' + amount + ']不能转换为整数');    
           amount = 0;    
        }    
        switch(field){    
           case this.DEFAULT_YEAR:    
             tmpdate.setFullYear(tmpdate.getFullYear() + amount);    
             break;    
           case this.DEFAULT_MONTH:    
             tmpdate.setMonth(tmpdate.getMonth() + amount);    
             break;    
           case this.DEFAULT_DATE:    
             tmpdate.setDate(tmpdate.getDate() + amount);    
             break;    
           case this.DEFAULT_HOUR:    
             tmpdate.setHours(tmpdate.getHours() + amount);    
             break;    
           case this.DEFAULT_MINUTE:    
             tmpdate.setMinutes(tmpdate.getMinutes() + amount);    
             break;    
           case this.DEFAULT_SECOND:    
              tmpdate.setSeconds(tmpdate.getSeconds() + amount);    
             break;    
           default:    
              this.debug('你输入的[interval:' + field + '] 不符合条件!');            
        }    
            
        this.curDate = tmpdate;    
        return this.formatCurrentDate(targetFormat == undefined? strFormat: targetFormat);    
    },    
        
    /**  
     * 比较两个日期的差距  
     * @param date1 Date类型的时间  
     * @param date2 Dete 类型的时间  
     * @param isFormat boolean 是否对得出的时间进行格式化,<br>   
     *       false:返回毫秒数，true：返回格式化后的数据  
     * @return 返回两个日期之间的毫秒数 或者是格式化后的结果  
     */    
    compareTo: function(date1, date2, isFormat){    
      try{    
            var len = arguments.length;    
            var tmpdate1 = new Date();    
            var tmpdate2 = new Date();    
            if(len == 1){    
               tmpdate1 = date1;    
            }else if(len >= 2){    
              tmpdate1 = date1;    
              tmpdate2 = date2;    
            }    
        if(!(tmpdate1 instanceof Date) || !(tmpdate2 instanceof Date)){    
           return 0;    
        }else{    
            var time1 = tmpdate1.getTime();     
            var time2 = tmpdate2.getTime();    
            var time = Math.max(time1, time2) - Math.min(time1, time2);    
            if(!isNaN(time) && time > 0){    
               if(isFormat){    
                  var date = new Date(time);    
                  var result = {};    
                  result['year']   = (date.getFullYear() - 1970) > 0? (date.getFullYear() - 1970): '0';    
                  result['month']  = (date.getMonth() - 1) > 0? (date.getMonth() - 1): '0';    
                  result['day']    = (date.getDate() - 1) > 0? (date.getDate() - 1): '0';    
                  result['hour']   = (date.getHours() - 8) > 0? (date.getHours() - 1): '0';    
                  result['minute'] = date.getMinutes() > 0? date.getMinutes(): '0';    
                  result['second'] = date.getSeconds() > 0? date.getSeconds(): '0';    
                  return result;    
                }else {    
                 return time;    
                }    
            }else{    
              return 0;    
            }    
        }    
      }catch(e){    
        this.debug('比较时间出现异常' + e.message);    
      }    
    },    
        
    /**  
     * 根据给定的日期得到日期的月，日，时，分和秒的对象  
     * @params date 给定的日期 date为非Date类型， 则获取当前日期  
     * @return 有给定日期的月、日、时、分和秒组成的对象  
     */    
    getDateObject: function(date){    
         if(!(date instanceof Date)){    
           date = new Date();    
         }    
        return {    
            'M+' : date.getMonth() + 1,     
            'd+' : date.getDate(),       
            'H+' : date.getHours(),       
            'm+' : date.getMinutes(),     
            's+' : date.getSeconds()    
         };    
    },    
        
    /**  
     *在控制台输出日志  
     *@params message 要输出的日志信息  
     */    
    debug: function(message){    
       try{    
           if(!this.isDebug){    
             return;    
           }    
           if(!window.console){    
               window.console = {};    
               window.console.log = function(){    
                  return;    
               }    
           }    
           window.console.log(message + ' ');    
       }catch(e){    
       }    
    }    
}   

var _config={};
//
//_config.url="http://gps.sztrackpro.com/";
var _data_url1="data.aspx";
var _data_url2="datalist.aspx";
var _carlist_isloading=false;
var _arrow=new Array("images/a0.png","images/a1.png","images/a2.png","images/a3.png","images/a4.png","images/a5.png","images/a6.png","images/a7.png");
var _pos_start="images/start.png";
var _pos_end="images/end.png";

var _icons;

var map;
var lat=-34.397;
var lng=150.644;
var marker;
var poly; 

var map_his;
var marker_his;
var poly_his;

var _islogin=false;
var _infownd;
//定义指令
//
var _CMD_OVERSPEEDALARM=0;
var _CMD_TRACKONE=1;
var _CMD_OIL_OFF=2;
var _CMD_OIL_ON=3;
var _CMD_SETBACKMOD=4;
var _CMD_POWER_OFF=56;
var _CMD_POWER_ON=57;
var _CMD_SENDMESSAGE=59;

google.maps.visualRefresh = true;
var GoogleGeocoder=null;

function initialize() {
	$('#map').height($(window).height()-$('#ss_map_header').height());
	var mapOptions = {
		zoom: 16,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	  };
	  
	map = new google.maps.Map(document.getElementById('map'),
		  mapOptions);
	  
	var polyOptions = {
		  strokeColor: '#ff0000',
		  strokeOpacity: 1.0,
		  strokeWeight: 3
	}
	poly = new google.maps.Polyline(polyOptions);
	poly.setMap(map);
	var _iconsize=new google.maps.Size(36, 36);
	var _anchor=new google.maps.Point(18, 18);
	//origin: new google.maps.Point(0,0),  
	_icons=[{url: _arrow[0],size:_iconsize,anchor:_anchor},
				{url: _arrow[1],size:_iconsize,anchor:_anchor},
				{url: _arrow[2],size:_iconsize,anchor:_anchor},
				{url: _arrow[3],size:_iconsize,anchor:_anchor},
				{url: _arrow[4],size:_iconsize,anchor:_anchor},
				{url: _arrow[5],size:_iconsize,anchor:_anchor},
				{url: _arrow[6],size:_iconsize,anchor:_anchor},
				{url: _arrow[7],size:_iconsize,anchor:_anchor}];

	marker = new google.maps.Marker({   
				icon: _icons[0],
				map: map
			});

	_infownd = new google.maps.InfoWindow();   

	google.maps.event.addListener(marker, 'click', function() {
			_infownd.open(map,marker);
	});
	
	google.maps.event.addListener(marker, 'click', function() {
		
	});

	
}

function initialize_his() {
	$('#his_map').height($(window).height()-$('#his_map_header').height());
	
	var mapOptions = {
		zoom: 12,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	  };
	
	
	map_his = new google.maps.Map(document.getElementById('his_map'),
		  mapOptions);

	var polyOptions = {
		  strokeColor: '#ff0000',
		  strokeOpacity: 1.0,
		  strokeWeight: 3
	}
	
	poly_his = new google.maps.Polyline(polyOptions);
	poly_his.setMap(map_his);
	var _iconsize=new google.maps.Size(36, 36);
	var _anchor=new google.maps.Point(18, 18);
	_icons=[{url: _arrow[0],size:_iconsize,anchor:_anchor},
				{url: _arrow[1],size:_iconsize,anchor:_anchor},
				{url: _arrow[2],size:_iconsize,anchor:_anchor},
				{url: _arrow[3],size:_iconsize,anchor:_anchor},
				{url: _arrow[4],size:_iconsize,anchor:_anchor},
				{url: _arrow[5],size:_iconsize,anchor:_anchor},
				{url: _arrow[6],size:_iconsize,anchor:_anchor},
				{url: _arrow[7],size:_iconsize,anchor:_anchor}];

	marker_his = new google.maps.Marker({   
				icon: _icons[0],
				map: map_his
			});
	
	var laglng=new google.maps.LatLng(30.37,114.21);
				map_his.setCenter(laglng);
	marker_his.setPosition(laglng);

	_infownd = new google.maps.InfoWindow();   

	google.maps.event.addListener(marker_his, 'click', function() {
			_infownd.open(map_his,marker_his);
	});
	
	google.maps.event.addListener(marker_his, 'click', function() {
		
	});	
}

server_timezone = 8;
var UtcDate = function(d)
{
	var time_offset = new Date().getTimezoneOffset() * 60000;
	var time_utc = d + time_offset + server_timezone * 60 * 60000;
	var date = new Date(time_utc);

	return date;
};


function login(){
	$.support.cors = true;
	$.mobile.allowCrossDomainPages = true;	 
	
	showLoading("login...");
	
    _carlist_isloading=false;
	$.ajax({
		type:'post',
		dataType : 'json',
		timeout:30000,
		url:_config.url+_data_url1+"?action=login",
		data:{username:$('#account').val(),userpass:$('#password').val()},
		success:function(data){
			if(data.success)
			{
				_config.account=$('#account').val();
				_config.password=$('#password').val();

				saveAccount($('#account').val(),$('#password').val());		
				$.mobile.navigate('#list_page',{transition:'none',changeHash: true });
				hideLoading();
			}
			else
			{
				hideLoading();
				alert("login failure...");
			}
		},
		error:function(jqXHR,txtStatus){
			hideLoading();
			//loginfailure();
			alert(jqXHR.statusText+"<br />"+jqXHR.responseText);
			//navigator.splashscreen.hide();	
		}
	});
}


function loginfailure(){
	//alert('11');
	//$.mobile.initializePage();
	//$.mobile.changePage('#login_page',{transition:'none',changeHash: false });
}

document.addEventListener("deviceready", function(){
	//navigator.splashscreen.show();	
	
	$.support.cors = true;
	$.mobile.allowCrossDomainPages = true;	 	
	$.mobile.buttonMarkup.hoverDelay = 100;

	window.fillConfig = function(callback) {
		cordova.exec(callback, null, "NotificationClient", "fillconfig",[]);
	};

	window.saveConfig = function(_url,callback) {
		cordova.exec(callback, null, "NotificationClient", "saveconfig",[_url]);
	};

	window.saveAccount = function(_account,_password,callback) {
		cordova.exec(callback, null, "NotificationClient", "saveaccount",[_account,_password]);
	};

	window.appExit = function() {
		cordova.exec(null, null, "NotificationClient", "appexit",[]);
	};

	var configstr = "";
	
	fillConfig(function(ret){
		_config=$.parseJSON(ret);

		if(_config.account!=undefined&&_config.account!="")
			$('#account').val(_config.account);
		if(_config.password!=undefined&&_config.password!="")
			$('#password').val(_config.password);
		if(_config.url!=undefined&&_config.url!="")
			$('#url').val(_config.url);
	});	

	//处理手机返回键
	
	document.addEventListener("backbutton", function(){
		//alert($.mobile.activePage.attr("id")+"."+$.mobile.activePage.attr("pagehash"));
		//如果当前页为车辆列表首页时，不会退回到登录页
		//而是默认为退出程序
		if($.mobile.activePage.attr("id")=="list_page"||$.mobile.activePage.attr("id")=="login_page")
		{
			//调用系统退出接口函数，询问是否退出
			//			confirmExit();
			navigator.notification.confirm("Exit the Application?", function(button){
				if(button==2)
				{
					appExit();
				}

			}, "Confirm", ["No","Yes"]);
		}
		else
			window.history.back();
	}, true);
	
	//navigator.splashscreen.hide();	
}, false);

//$.mobile.autoInitializePage=false;

$(document).bind("mobileinit",function(){
	
});

var _carid=0;
var _interval=0;
var _online_interval=0;
var _play_interval=0;

var _car;
var _postimes=0;
$(document).on("pageinit","#login_page",function(){
	//navigator.splashscreen.show();	
	//离开list_page后，关闭车辆状态维护，以节约资源
	$("#list_page").on("pagehide",function(event){
		if(_online_interval!=0)
		{
			clearInterval(_online_interval);
			_online_interval=0;
		}
	});

	//
	$("#list_page").on( "pageshow", function( event ) {
		$("#list_header").html('['+_config.account+'] MyGroup');
		
		if(!_carlist_isloading)
			getCarList();
		else
		{
			//重新开始车辆状态维护
			_online_interval=setInterval(getOnline,1000);
		}
	});

	$("#map_page").on("pageshow", function( event ) {
		if(!map)
			initialize();

		//querygpsdata
		var path = poly.getPath();    
		path.clear();
		
		getCarInfo();
		_postimes=0;
		_interval = setInterval(getCarPos,5000);
	});	

	$("#map_page").on("pagehide",function(event){
		stopCarPos();
	});

	$('#history_page').on("pageshow",function(event){
		if(!map_his)
			initialize_his();

		var path = poly_his.getPath();    
		path.clear();		

		if(_play_interval!=0)
		{
			clearInterval(_play_interval);
			_play_interval=0;
		}

		getCarInfo();
		var _start=$('#his_start_date').val();
		var _end=$('#his_end_date').val();
		getCar_History_pos(_start,_end,1);
	});

	$("#act_page").on("pagehide",function(event){
		if(_trackinterval>0)
		{
			clearInterval(_trackinterval);
			_trackinterval=0;
		}
	});

	$("#option_page").on("pageshow",function(event){
		if(_config.url==undefined||_config.url=='')
		{
			option();
		}
	});

	var opt = {
        preset: 'date', //日期
        theme: 'Sense UI', //皮肤样式
        mode: 'clickpick', //日期选择模式
        dateFormat: 'yyyy-mm-dd', // 日期格式
        dateOrder: 'yyyymmdd', //面板中日期排列格式
		display:'Modal',
		Lang:'English'
    };
    
    $('input:jqmData(role="datebox")').mobiscroll(opt);
	
	var _startdate=new DateFormat();
	var _enddate=new DateFormat();
	_startdate=_startdate.changeDate(new Date(),'DATE',-1,'yyyy-MM-dd','yyyy-MM-dd');
	$('#his_start_date').val(_startdate);
	$('#his_end_date').val(_enddate.formatCurrentDate('yyyy-MM-dd'));
});

function option(){
	$.mobile.changePage('#option_page');
}

function viewInfo(){
	getCarInfo();
}

function getCarInfo()
{
	if(_carid==0) return;
	//showLoading("loading...");

	$.ajax({
		type:'get',
		dataType : 'json',
		url:_config.url+_data_url2+"?action=cars_get2",
		data:{carid:_carid},
		success:function(data,textStatus,jqXHR){
			if(data.success)
			{
				//alert(jqXHR.responseText);
				_car=data.data;
				//$("#info").html(_html);
				//$.mobile.changePage('#info_page',{role:'dialog'});
				_html="carID:"+_car.carID+"<br />";
				_html+="car NO:"+_car.carNO+"<br />";
				_html+="michineNO:"+_car.machineNO+"<br />";
				
				//_html+="Address:"+_car.driverAddress+"<br />";				

				_infownd.setContent(_html); 
				//_infownd.open(map,marker); 
			}
			else
			{
				hideLoading();
			}
		},
		error:function(jqXHR,txtStatus){
			hideLoading();
			//alert(jqXHR.statusText+"<br />"+jqXHR.responseText);
		}
	});
}


function stopCarPos(){
	var path = poly.getPath();    
	path.clear();

	clearInterval(_interval);
}

function getCarPos()
{
	if(_carid==0) return;
	
	$.ajax({
		type:'post',
		dataType : 'json',
		url:_config.url+_data_url1+"?action=querygpsdata2",
		data:{carid:_carid},
		success:function(data,textStatus,jqXHR){
			//alert(jqXHR.responseText);
			if(data.success)
			{
				var laglng=new google.maps.LatLng(data.data[0].la, data.data[0].lo);
				map.setCenter(laglng);
				var path = poly.getPath();    
				path.push(laglng);
				
				marker.setIcon(_icons[data.data[0].direction]);
				marker.setPosition(laglng);
				
				var _ht="";
				_ht+="Date&Time:"+data.data[0].gpsTime_str+"<br />";
				_ht+="Speed:"+data.data[0].speed+"km/h &nbsp;&nbsp;";
				_ht+="Lat:"+data.data[0].la+"&nbsp;&nbsp;";
				_ht+="Lon:"+data.data[0].lo+"&nbsp;&nbsp;<br />";
				_ht+="NO:"+_car.carNO+"&nbsp;&nbsp;";

				if(_postimes==0)
				{
					/*GoogleGeocoder = new google.maps.Geocoder();
					GoogleGeocoder.geocode({"location": new google.maps.LatLng(data.data[0].la, +data.data[0].lo, true) }, function (results, status) {
							if(status != google.maps.GeocoderStatus.OK){
								_ht+="Address:" + results[0].formatted_address;
							}
					});
					$('#map_pos').html(_ht);*/

					//读取地址信息
					//http://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&sensor=true_or_false
					$.ajax({
						url:'get',
						dataType:'json',
						url:'http://ditu.google.cn/maps/api/geocode/json?latlng='+data.data[0].la+','+data.data[0].lo+'&sensor=true&language=en',
						success:function(data)
						{
							if(data.status=='OK')
							{
								//alert(data.results[0].formatted_address);
								_ht+="Address:"+data.results[0].formatted_address;
							}
							$('#map_pos').html(_ht);
						},
						error:function(){
							$('#map_pos').html(_ht);
						}
					});
				}
				else
				{
					$('#map_pos').html(_ht);
				}
				
				//由于geocode有调用次数限制，因此，每60s取一次地址
				_postimes=_postimes==60?0:_postimes++;
			}
		},
		error:function(jqXHR,txtStatus){
		}
	});
}

var _his_data=[];
var _stop=false;
var _index=0;

function getCar_History_pos(_startdate,_enddate,_type){
	if(_carid==0) return;
	showLoading("load history....");
	GetServerData("action=querygpshistorydata2",
		{carid:_carid,timestart:_startdate,timeend:_enddate,type:_type},
		function(data,textStatus,jqXHR){
			if(data.success)
			{
				var marker_start = new google.maps.Marker({   
					icon: _pos_start,
					map: map_his
				});
				
				var marker_end = new google.maps.Marker({   
					icon: _pos_end,
					map: map_his
				});
				
				hideLoading();
				var laglng=new google.maps.LatLng(data.data[0].la, data.data[0].lo);
				map_his.setCenter(laglng);				
				marker_his.setPosition(laglng);
				marker_start.setPosition(laglng);

				var endlng=new google.maps.LatLng(data.data[data.data.length-1].la, data.data[data.data.length-1].lo);
				marker_end.setPosition(endlng);

				var path = poly_his.getPath();    

				var _i=0;
				$(data.data).each(function(){
					if(_i==0)
					{
						_i++;
					}
					var laln=new google.maps.LatLng(this.la, this.lo);
					path.push(laln);
				});
				_his_data=data.data;
				//play
				_stop=true;
				_index=0;
			}
		},
		function(jqXHR,textStatus){
			hideLoading();
		});
}

function playHistory(){
	if(_stop || _index>_his_data.length-1) {

		$('#playbtn .ui-btn-text').text('play');
		$('#playbtn').buttonMarkup("refresh");

		return;
	}
	var _thisdata=_his_data[_index];
	_index++;

	var laln=new google.maps.LatLng(_thisdata.la, _thisdata.lo);
	map_his.setCenter(laln);				
	marker_his.setPosition(laln);
	marker_his.setIcon(_icons[_thisdata.direction]);

	setTimeout(playHistory,200);
}

function playorstop(){
	if(_stop){
		_stop=false;
		if(_index>_his_data.length-1)
			_index=0;
		
		$('#playbtn .ui-btn-text').text('pause');
		$('#playbtn').buttonMarkup("refresh");
		playHistory();
	}
	else{
		_stop=true;
		$('#playbtn .ui-btn-text').text('play');
		$('#playbtn').buttonMarkup("refresh");
	}
}

function loadMap(carId,obj){
	_carid=carId;
	
	$.mobile.navigate('#act_page',{transition:'none',changeHash: true});
}

function getOnline(){
	$.ajax({
		type:'post',
		dataType : 'json',
		url:_config.url+_data_url2+"?action=cars_onlinestate",
		success:function(data){
			if(data.success)
			{
				$(data.rows).each(function(){
					var _thisid=this.carID;
					if(this.isonline==0)
					{
						$('#on_'+this.carID).attr('src','images/offline.png');
					}
					else
					{
						$('#on_'+this.carID).attr('src','images/online.png');
					}
				});
			}
			else
			{
			}
		},
		error:function(jqXHR,txtStatus){
		}
	});
}

function getCarList(){

	showLoading("Get Car List....");
	$.ajax({
		type:'post',
		dataType : 'json',
		url:_config.url+_data_url2+"?action=tree_groupcars2",
		success:function(data){
			if(data)
			{
				_carlist_isloading=true;
				//$('#car_list').empty();
                $('#car_list *').remove();
				_group="";
				_group=createTree(data);

				$("#car_list").append(_group);
				$("#car_list").listview('refresh');
				hideLoading();
				//启动在线状态获取定时器
				_online_interval=setInterval(getOnline,1000);
			}
			else
			{
				hideLoading();
				alert("get car list failure...");
			}
		},
		error:function(jqXHR,txtStatus){
			hideLoading();
			alert(jqXHR.responseText);
		}
	});
}

function createTree(data){
	if($(data)==null||$(data).length==0) return "";
	var _html="";
	$(data).each(function(){
		if(this.type==0)
		{
			_html+='<li><a href="#"><img src="images/group.png" class="ui-li-icon ui-corner-none" />'+this.text_old+'<span class="ui-li-count">'+this.car_count+'</span></a>';
			if(this.children.length>0)
			{
				_html+='<ul data-mini="true">';
				_html+=createTree(this.children);
				_html+='</ul>';
			}
			_html+='</li>';
		}
		else
		{
			//_html+='<li><a onclick="loadMap('+this.objid+');" id="li_'+this.objid+'" data-rel="popup" data-native-menu="false" data-transition="none" ><img id="on_'+this.objid+'" src="images/offline.png" class="ui-li-icon ui-corner-none" />'+this.text+'</a></li>';
			_html+='<li><a onclick="loadMap('+this.objid+',this);" id="li_'+this.objid+'" data-transition="none" ><img id="on_'+this.objid+'" src="images/offline.png" class="ui-li-icon ui-corner-none" />'+this.text+'</a></li>';
		}
	});

	return _html;
}
//
function configIsInit(){
	return (_config.url!=undefined&&_config.url!="")&&(_config.account!=undefined&&_config.account!="")&&(_config.password!=undefined&&_config.password!="");
}

$(document).on("pageinit","#option_page",function(){
	
});

function data_action(action,success,failure){
	
}

function save_option(){
	if($('#url').val()!="")
	{
		showLoading("saving...");
		var url=$('#url').val();
		if(url.substr(url.length-1,1)!='/')
			url+='/';

		if(url.substring(0,7)!="http://")
			url="http://"+url;

		_config.url=url;
		saveConfig(url);
		hideLoading();
		window.history.back();
	}
	else
		alert("Url Is Not Empty");
}

function showLoading(message){
	$.mobile.loading( 'show', {
		textVisible: true,
		theme: 'a',
		text:message,
		textonly:false
	});
}

function hideLoading(){
	$.mobile.loading('hide');
}

function closespeedlimt(){
	$('#speedlimit').popup('close');
}

function closesetinterval(){
	$('#setinterval').popup('close');
}

function closemessage(){
	$('#sendmessage').popup('close');
}

var _trackinterval=0;

function sendcmd(_cmd)
{
	var p = {};
	p.username = _config.account;
	p.cmd = _cmd;
	p.carid = _carid;
	
	switch(_cmd)
	{
		case _CMD_SENDMESSAGE:
		{
			p.msg=$('message').val();
			showLoading("Sending...");
			GetServerData("action=order_send",p,
				function(data){
					showInfo("The message is sended!");
					closemessage();
				},
				function(jqXHR,txtStatus){
					alert(jqXHR.responseText);
			});
			break;
		}
		case _CMD_OVERSPEEDALARM:				//超速限制
		{
			p.value=$('#sl_val').val();
			showLoading("Sending...");
			GetServerData("action=order_send",p,
				function(data){
					showInfo("The speed limit setting is succuess!");
					closespeedlimt();
				},
				function(jqXHR,txtStatus){
					alert(jqXHR.responseText);
			});
			break;
		}
		case _CMD_SETBACKMOD:
		{
			p.value=$('#timer_val').val();
			showLoading("Sending...");
			GetServerData("action=order_send",p,
				function(data){
					showInfo("The interval setting is succuess!");
					closesetinterval();
				},
				function(jqXHR,txtStatus){
					alert(jqXHR.responseText);
			});
			break;
		}
		case _CMD_OIL_OFF:					//断油
		{
			navigator.notification.confirm(
				'Confirm Send This Command?', // message
				 function(btn){
					 if(btn==2)
					 {
						 showLoading("Sending...");
						GetServerData("action=order_send",p,
							function(data){
								showInfo("Oil disabled!");
							},
							function(jqXHR,txtStatus){
								alert(jqXHR.responseText);
						});
					 }
				 },          
				'Confirm',   
				'Cancel,Ok'
			);
			break;
		}
		case _CMD_OIL_ON:						//开油
		{
			navigator.notification.confirm(
				'Confirm Send This Command?', // message
				 function(btn){
					 if(btn==2)
					 {
						 showLoading("Sending...");
						GetServerData("action=order_send",p,
							function(data){
								showInfo("Oil enabled!");
							},
							function(jqXHR,txtStatus){
								alert(jqXHR.responseText);
						});
					 }
				 },          
				'Confirm',   
				'Cancel,Ok'
			);
			break;
		}
		case _CMD_POWER_OFF:				//断电
		{
			navigator.notification.confirm(
				'Confirm Send This Command?', // message
				 function(btn){
					 if(btn==2)
					 {
						 showLoading("Sending...");
						GetServerData("action=order_send",p,
							function(data){
								showInfo("Power disabled!");
							},
							function(jqXHR,txtStatus){
								alert(jqXHR.responseText);
						});
					 }
				 },          
				'Confirm',   
				'Cancel,Ok'
			);
			break;
		}
		case _CMD_POWER_ON:
			{
			navigator.notification.confirm(
				'Confirm Send This Command?', // message
				 function(btn){
					 if(btn==2)
					 {
						 showLoading("Sending...");
						GetServerData("action=order_send",p,
							function(data){
								showInfo("Power enabled!");
							},
							function(jqXHR,txtStatus){
								alert(jqXHR.responseText);
						});
					 }
				 },          
				'Confirm',   
				'Cancel,Ok'
			);
			break;
		}
		case _CMD_TRACKONE:
		{
			//showInfo("Locate is succuess!Car Position is LAT");

			navigator.notification.confirm(
				'Confirm Send This Command?', // message
				 function(btn){
					 if(btn==2)
					 {
						showLoading("Sending...");

						GetServerData("action=order_send",p,
							function(data){
								if(_trackinterval==0)
								{
									_trackinterval=setInterval(function(){
										GetServerData("action=querygpsdata2",{carid:_carid},function(data,jqXHR){
											showInfo("Date&Time:"+data.data[0].gpsTime_str+"&nbsp;&nbsp; Speed:"+data.data[0].speed+"km/h<br />LAT:"+data.data[0].la+"&nbsp;LON:"+data.data[0].lo+"<br />Car Status:"+data.data[0].status);
										},null);
										},
									1000);
								}
							},
							function(jqXHR,txtStatus){
								//alert(jqXHR.responseText);
						});
					 }
				 },          
				'Confirm',   
				'Cancel,Ok'
			);
			break;
		}
	}	
}

function showInfo(message){

	hideLoading();
	//var _html='<a data-role="button" data-theme="e" data-corners="false" data-shadow="false" data-mini="true">'+message+'</a>';
	$("#cmd_info_div").html(message);

	//$('#cmd_info').html(message);
	//$('#cmdinfo').popup("open",{transition:'pop'});
	/*
	$('#cmd_info_loading').loading('show', {
		text: message,
		textVisible: true,
		textonly:true,
		theme: 'e',
		html: ""
	});
	
	setTimeout(function(){$('#cmd_info_loading').loading('hide');},1000);*/
}

//与服务器端通信
var GetServerData = function (parame_get, parame_post, fun_success, fun_failure) {
    var _method = "GET";
    var _url = _config.url+_data_url1;
    var _params = parame_post;

    if (parame_post) _method = "POST";
    if (parame_get) _url += "?" + parame_get;

    $.ajax({
		timeout: 600000,
        type: _method,
        headers: { "charset": "UTF-8" },
        url: _url,
        data: _params,
		dataType:'json',
        success: function (res,textStatus,jqXHR) {
            if (res && fun_success) {
                fun_success(res,jqXHR);
            }
            else if (fun_failure) {
                fun_failure();
            }
        },
        error: function (jqXHR,txtStatus) {
            if (fun_failure) { fun_failure(jqXHR,txtStatus); }
        }
    });
};