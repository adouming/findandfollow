//
//  MyConfig.m
//  GPSTrack
//
//  Created by hubo on 13-9-24.
//
//

#import "MyConfig.h"

NSString* config_url = @"";
NSString* config_account = @"";
NSString* config_password = @"";

@implementation MyConfig

+ (void)GetConfig{
    config_url = [[NSUserDefaults standardUserDefaults] stringForKey:@"setting_url"];
    config_account = [[NSUserDefaults standardUserDefaults] stringForKey:@"setting_account"];
    config_password = [[NSUserDefaults standardUserDefaults] stringForKey:@"setting_password"];
    
    if (config_url == nil || [config_url length] == 0) {
        config_url = @"http://27.50.87.150/";
    }
    if (config_account == nil || [config_account length] == 0) {
        config_account = @"";
    }
    if (config_password == nil || [config_password length] == 0) {
        config_password = @"";
    }
}

+ (void)SaveURL:(NSString*)url{
    config_url = url;
    [[NSUserDefaults standardUserDefaults] setObject:config_url forKey:@"setting_url"];
}

+ (void)SaveAccount:(NSString*)account{
    config_account = account;
    [[NSUserDefaults standardUserDefaults] setObject:config_account forKey:@"setting_account"];
}

+ (void)SavePassword:(NSString*)password{
    config_password = password;
    [[NSUserDefaults standardUserDefaults] setObject:config_password forKey:@"setting_password"];
}

@end
