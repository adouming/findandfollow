//
//  MyConfig.h
//  GPSTrack
//
//  Created by hubo on 13-9-24.
//
//

#import <Foundation/Foundation.h>

extern NSString* config_url;
extern NSString* config_account;
extern NSString* config_password;

@interface MyConfig : NSObject

+ (void)GetConfig;
+ (void)SaveURL:(NSString*)url;
+ (void)SaveAccount:(NSString*)account;
+ (void)SavePassword:(NSString*)password;

@end
