//
//  NotificationClient.h
//  GPSTrack
//
//  Created by hubo on 13-9-24.
//
//

//#import <Cordova/Cordova.h>
#import <Cordova/CDV.h>

@interface NotificationClient : CDVPlugin

/*
- (void) nativeFunction:(NSMutableArray*)arguments withDict:(NSMutableDictionary*)options;
- (void) myPluginMethod:(CDVInvokedUrlCommand*) command;
*/

- (void) alert:(CDVInvokedUrlCommand*) command;
- (void) fillconfig:(CDVInvokedUrlCommand*) command;
- (void) saveconfig:(CDVInvokedUrlCommand*) command;
- (void) saveaccount:(CDVInvokedUrlCommand*) command;

@end
