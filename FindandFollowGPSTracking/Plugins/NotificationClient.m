//
//  NotificationClient.m
//  GPSTrack
//
//  Created by hubo on 13-9-24.
//
//

#import "MyConfig.h"
#import "NotificationClient.h"

@implementation NotificationClient

/*
- (void) nativeFunction:(NSMutableArray*)arguments withDict:(NSMutableDictionary*)options {
    
    //get the callback id
    NSString *callbackId = [arguments pop];
    
    NSLog(@"Hello, this is a native function called from PhoneGap/Cordova!");
    
    NSString *resultType = [arguments objectAtIndex:0];
    CDVPluginResult *result;
    
    NSLog(resultType);
    
    if ( [resultType isEqualToString:@"success"] ) {
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString: @"Success :)"];
        [self writeJavascript:[result toSuccessCallbackString:callbackId]];
    }
    else {
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString: @"Error :("];
        [self writeJavascript:[result toErrorCallbackString:callbackId]];
    }
}

- (void) myPluginMethod:(CDVInvokedUrlCommand*) command{
    CDVPluginResult* pluginResult = nil;
    NSString* echo = [command.arguments objectAtIndex:0];
    
    NSLog(@"哈哈, this is a native function called from PhoneGap/Cordova!");
    
    if (echo != nil && [echo length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}


*/




- (void) alert:(CDVInvokedUrlCommand*) command{
    CDVPluginResult* pluginResult = nil;
    NSString* _title = [command.arguments objectAtIndex:0];
    NSString* _msg = [command.arguments objectAtIndex:1];
    
    [[[UIAlertView alloc] initWithTitle:_title message:_msg delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil] show];
    
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"ok"];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) fillconfig:(CDVInvokedUrlCommand*) command{
    NSString* res = [NSString stringWithFormat:@"{\"url\":\"%@\",\"account\":\"%@\",\"password\":\"%@\"}", config_url, config_account, config_password];
    NSLog(@"获取配置参数:%@", res);
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:res];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) saveconfig:(CDVInvokedUrlCommand*) command{
    CDVPluginResult* pluginResult = nil;
    NSString* _url = [command.arguments objectAtIndex:0];
    
    [MyConfig SaveURL:_url];
    
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"ok"];
    NSLog(@"保存网址成功");
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) saveaccount:(CDVInvokedUrlCommand*) command{
    CDVPluginResult* pluginResult = nil;
    NSString* _account = [command.arguments objectAtIndex:0];
    NSString* _password = [command.arguments objectAtIndex:1];
    
    [MyConfig SaveAccount:_account];
    [MyConfig SavePassword:_password];
    
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"ok"];
    NSLog(@"保存用户名密码成功");
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}



@end
